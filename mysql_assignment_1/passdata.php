<?php
//start the session
session_start();

if (!isset($_SESSION["name"])) {
  echo "session destroy";
} else {
?>
  <!DOCTYPE html>
  <html lang="en" dir="ltr">
  <html>

  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Resume</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- External CSS -->
    <link rel="stylesheet" href="resume.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css" integrity="sha512-ARJR74swou2y0Q2V9k0GbzQ/5vJ2RBSoCWokg4zkfM29Fb3vZEQyv0iWBMW/yvKgyHSR/7D64pFMmU8nYmbRkg==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  </head>

  <body>
    <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#"><?php echo $_SESSION["name"]; ?></a>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="#about">About Me</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#experience">Experience</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#education">Education</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#contact">Contact</a>
          </li>
        </ul>
      </div>
    </nav>


    <!--| ABOUT |--------------------------------------------------->
    <section id="about" class="container">
      <img src=<?php echo "upload/" . $_SESSION["photo"]; ?> alt="">
      <h1 class="display-4"><?php echo $_SESSION["name"]; ?></h1>
      <p>
        <?php echo $_SESSION["about"]; ?>
      </p>
      <p>
        <strong>Skills:</strong>
        <span class="badge badge-info ">
          <?php echo $_SESSION["skill"]; ?></span>
      </p>
    </section>

    <!-- EXPERIENCE --------------------------------------------->
    <section id="experience" class="container">
      <h1>Experience</h1>
      <div class="card">
        <div class="card-header collapse show" data-toggle="collapse" data-target="#exp1">
          <div class="row">
            <h5 class="col-md-8 mb-0">Mindfire Solutions</h5>
            <em class="col-md-4 text-md-right">18 january-Current</em>
          </div>
        </div>
        <div class="card-block collapse" id="exp1">
          <h5>Web Developer</h5>
          <p>
            Leveraged agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.
          </p>
          <p>
            Brought to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a streamlined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring.
          </p>
          <p>
            Capitalized on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.
          </p>
        </div>
      </div>

      <!-- EDUCATION QUALIFICATION--------------------------------------------->
      </div>
    </section>
    <section id="experience" class="container">
      <h1>Education Qualification</h1>
      <div class="card">
        <div class="card-header  show">
          <div class="row">
            <h5 class="col-md-8 mb-0"><?php echo $_SESSION["education"]; ?></h5>
            <em class="col-md-4 text-md-right"></em>
          </div>
        </div>
      </div>


      </div>
    </section>
    </section>
    <section id="experience" class="container">
      <h1>Interests</h1>
      <div class="card">
        <div class="card-header  show">
          <div class="row">
            <h5 class="col-md-8 mb-0"> <?php echo $_SESSION["interest"]; ?></h5>
            <em class="col-md-4 text-md-right"></em>
          </div>
        </div>
      </div>
    </section>

    <!-- CONTACT ----------------------------------------------------->

    <section class=" //redirect to login page
  header('Location: ../index.php');container" id="contact">
      <h1>Contact</h1>
      <div class="row">
        <div class="col-sm-2">Phone:</div>
        <div class="col-sm-4"><?php echo $_SESSION["mobile"]; ?></div>
      </div>
      <div class="row">
        <div class="col-sm-2">Email:</div>
        <div class="col-sm-4"><a href="url"><?php echo $_SESSION["email"]; ?></a></div>
      </div>
      <div class="row">
        <div class="col-sm-2">Education :</div>
        <div class="col-sm-4"><?php echo $_SESSION["education"]; ?></div>
      </div>
      <div class="row">
        <div class="col-sm-2">Gender:</div>
        <div class="col-sm-4"><?php echo $_SESSION["gender"]; ?></div>
      </div>
      <div class="row">
        <div class="col-sm-2">Date of birth:</div>
        <div class="col-sm-4"><?php echo $_SESSION["date"]; ?></div>
      </div>
      <div class="row">
        <div class="col-sm-2">Website:</div>
        <div class="col-sm-4"><a href="url"><?php echo $_SESSION["prof"]; ?></a></div>
      </div>
      <div class="row">
        <div class="col-sm-2">Address::</div>
        <div class="col-sm-4"><?php echo $_SESSION["address"]; ?></div>
      </div>
    </section>
    <!----------------------script------------------------------>
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
  </body>

  </html>
<?php
  //session_unset();
  session_destroy();
}
?>