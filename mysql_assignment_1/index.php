<?php
//start the session
session_start();
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Registration Page</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <!-- External CSS -->
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css" integrity="sha512-ARJR74swou2y0Q2V9k0GbzQ/5vJ2RBSoCWokg4zkfM29Fb3vZEQyv0iWBMW/yvKgyHSR/7D64pFMmU8nYmbRkg==" crossorigin="anonymous" />
</head>

<body>

  <?php

  //PHP VALIDATION CODE
  $name_error = $email_error = $gender_error = $skill_error =  $date_error = $about_error = $address_error = $mobile_error = $education_error = $profile_error = $prof_error =  $output =   $interestErr = "";
  $error = "false";
  $name = $about = $contact = $email = $date = $skill = $education = $interest = $gender = $address = $profile = $prof = "";
  $error = 'false';


  if (isset($_POST["submit"])) {

    //USERNAME VALIDATION 
    if (empty($_POST["name"])) {
      $name_error = "<p>Please enter your name</p>";
      $error = 'true';
    } else {
      $name = test_input($_POST["name"]);
      if (!preg_match("/^[a-zA-Z ]*$/", $_POST["name"])) {
        $name_error = "<p>Only Letter are allowed</p>";
        $error = 'true';
      }
    }

    // email validation
    if (empty($_POST["email"])) {
      $email_error = "<p>Please enter your email id</p>";
      $error = 'true';
    } else {
      $email = test_input($_POST["email"]);
      if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
        $email_error = "<p>Invalid email format</p>";
        $error = 'true';
      }
    }

    // Gender validation
    if (empty($_POST["gender"])) {
      $gender_error = "<p>Gender is required<p>";
      $error = 'true';
    } else {
      $gender = test_input($_POST["gender"]);
    }

    //mobile number validation
    if (empty($_POST["mobile"])) {
      $mobile_error = "<p>Please enter your Mobile no</p>";
      $error = 'true';
    } else {
      $mobile = test_input($_POST["mobile"]);
      if (!preg_match('/^[6-9]\d{9}$/',  $_POST["mobile"])) {
        $mobile_error = "<p>Invalid mobile format</p>";
        $error = 'true';
      } else {
        $mobile_error = "";
      }
    }

    //skills validation
    if (empty($_POST["skill"])) {
      $skill_error = "<p>You must select a skill</p>";
      $error = 'true';
    }

    if (!empty($_POST['skill'])) {
      $skills = $xx = $_POST['skill'];
      $no_checked = count($_POST['skill']);
      if ($no_checked < 2) {
        $skill_error = "Select at least two options";
        $error = 'true';
      } else {
        for ($i = 0; $i < $no_checked; $i++) {
          $skill = $skill . " " . $xx[$i];
        }
      }
    }

    //Address Validation
    if (empty($_POST["address"])) {
      $address_error = "Please enter your address";
      $error = 'true';
    } else {
      $address = test_input($_POST["address"]);
      if (strlen($address) <= 3) {
        $address_error = "More than three characters required.";
        $error = 'true';
      }
    }


    //Date of birth validation
    if (empty($_POST["date"])) {
      $date_error = "<p>Please enter your date of birth<p>";
      $error = 'true';
    } else {
      $date = test_input($_POST["date"]);
    }

    //About Validation
    if (empty($_POST["about"])) {
      $about_error = "<p>Please enter about you</p>";
      $error = 'true';
    } else {
      $about = test_input($_POST["about"]);
      if (strlen($about) <= 10) {
        $about_error = "<p>More than ten characters required.</p>";
        $error = 'true';
      }
    }

    //Education validation
    if (empty($_POST["education"])) {
      $education_error = "<p>Please select your branch<p>";
      $error = 'true';
      $ad = (strlen($_POST["address"]));
    } else {
      $education = test_input($_POST["education"]);
    }

    //interest validation
    if (empty($_POST["interest"])) {
      $interestErr = "<p>Please select your interest</p> ";
      $error = 'true';
    }


    if (!empty($_POST['interest'])) {

      $interests = $xx = $_POST['interest'];
      $no_checked = count($_POST['interest']);

      if ($no_checked < 2) {
        $interestErr = "<p>Select at least two options</p>";
        $error = 'true';
      } else {
        for ($i = 0; $i < $no_checked; $i++) {
          $interest = $interest . " " . $xx[$i];
        }
      }
    }


    //Professional validation
    if (empty($_POST["prof"])) {
      $prof_error = "<p>Please provide the link</p>";
      $error = 'true';
    } else {
      $prof = test_input($_POST["prof"]);
      if (!filter_var($_POST["prof"], FILTER_VALIDATE_URL)) {
        $prof_error = "<p>Invalid url</p>";
        $error = 'true';
      } else {
        $name_error = "";
      }
    }

    //profile pic validation
    if (!empty($_FILES["photo"]["name"])) {
      if ($_FILES["photo"]["error"] == 0) {
        $allowed_types = array("image/jpeg", "image/jpg", "image/png", "image/gif");

        if ((in_array($_FILES["photo"]["type"], $allowed_types))) {
          $dot_pos = strrpos($_FILES["photo"]["name"], ".");
          $extension = substr($_FILES["photo"]["name"], $dot_pos);
          $random_name = date("YmdHis");
          $new_name = $random_name . $extension;
          if ($_FILES["photo"]["size"] < 819200) {
            $photo = $new_name;
          } else {
            $error = 'true';
            $photoErr = "<p>Image should be less than 10KB </p>" . $_FILES["photo"]["size"];
          }
        } else {
          $error = 'true';
          $photoErr = "<p>Please upload JPG or PNG files</p>";
        }
      } else {
        $error = 'true';
        $photoErr = "<p>There are some errors with the file</p>";
      }
    } else {
      $error = 'true';
      $photoErr = "<p>Please browse a image to upload</p>";
    }

    //message 
    if (empty($_POST["message"])) {
      $message = "";
    } else {
      $message = test_input($_POST["message"]);
    }

    //output 
    if ($prof_error == "" && $interestErr == "" && $name_error == "" && $email_error == "" && $gender_error == "" && $skill_error == "" && $mobile_error == "" && $address_error == "" && $education_error == "" && $profile_error == "" && $about_error == "" && $date_error == "") {
      $_SESSION["name"] = $name;
      $_SESSION["about"] = $about;
      $_SESSION["mobile"] = $mobile;
      $_SESSION["email"] = $email;
      $_SESSION["date"] = $date;
      $_SESSION["skill"] = $skill;
      $_SESSION["education"] = $education;
      $_SESSION["interest"] = $interest;
      $_SESSION["prof"] = $prof;
      $_SESSION["gender"] = $gender;
      $_SESSION["address"] = $address;
      $_SESSION["photo"] = $photo;
      //VALUE SENDING TO NEXT PAGE FOR GET

      header('Location: mysql.php?name=' . $name . '&email=' . $email . '&address=' .  $address . '&date=' . $date . '&photo=' . $photo .  '&mobile=' . $mobile . '&prof=' . $prof . '&education=' . $education . '&prof=' . $prof .  '&gender=' . $gender .  '&about=' . $about .  '&about=' . $about . '&skill=' . $skill . '&interest=' . $interest);
    }
  }
  function test_input($data)
  {
    $data = trim($data);
    $data = stripcslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
  ?>
  <div class="container mt-2">
    <h3 style="text-align: center; font-weight: bold; font-size: 25px;">PHP Form Validation</h3>
    <div class="row ">
      <div class="col-md-6 offset-md-3 jumbotron">
        <div class="card card-body">
          <form class="myfrom" method="post" enctype="multipart/form-data">

            <!------------------------user name-------------------->
            <div class="form-group">
              <label for="full_name">Full Name</label>
              <input type="text" name="name" id="full_name" class="form-control" value="<?= $name ?>" placeholder="Enter your name">
              <span class="text-danger"> <?php echo $name_error; ?> </span>
            </div>

            <!-----------------------gmail-------------------->
            <div class="form-group">
              <label for="full_email">Email Address</label>
              <input type="Email" id="full_email" name="email" class="form-control" value="<?= $email ?>" placeholder="Email Address">
              <span class="text-danger"> <?php echo $email_error; ?> </span>
            </div>

            <!------------------------date of birth-------------------->
            <div class="form-group">
              <label>Date of birth</label><br>
              <input type="date" name="date" id="txtDate" value="<?= $date ?>"><br>
              <span id="lblError" style="color:Red"></span>
              <span class="text-danger"> <?php echo $date_error; ?> </span>
            </div>

            <!------------------------Contact number-------------------->
            <div class="form-group">
              <label for="mobile">Mobile no</label>
              <input type="text" id="mobile" onkeypress="phoneno()" name="mobile" value="<?= $mobile ?>" class="form-control" placeholder="Mobile no">
              <span class="text-danger"> <?php echo $mobile_error; ?> </span>
            </div>

            <!------------------------gender-------------------->
            <div class="form-group">
              <label>Gender</label><br>
              <label class="radio-inline"><input type="radio" name="gender" value="male" <?php echo ($_POST["gender"] == "Male") ? 'checked' : ''; ?>>Male</label>
              <label class="radio-inline"><input type="radio" name="gender" value="female" <?php echo ($_POST["gender"] == "female") ? 'checked' : ''; ?>>Female</label>
              <label class="radio-inline"><input type="radio" name="gender" value="others" <?php echo ($_POST["gender"] == "others") ? 'checked' : ''; ?>>Others</label>
              <span class="text-danger"> <?php echo $gender_error; ?> </span>
            </div>

            <!------------------------about-------------------->
            <div class="form-group">
              <label for="about">About</label>
              <textarea class="form-control" id="word1" name="about" oninput="countWord()" id="about" rows="3" maxlength="60" placeholder="Enter some data about youself"><?php echo $about ?></textarea>
              <span class="text-danger"> <?php echo $about_error; ?> </span>
              <p style="color:aqua;"> Word Count:
                <span id="show1" style="color:red;">60</span>
              </p>
            </div>

            <!------------------------address-------------------->
            <div class="form-group">
              <label for="address">Address</label>
              <textarea class="form-control" id="word" oninput="countWord()" id="address" rows="3" name="address" placeholder="Enter your Address" maxlength="100"><?php echo $address ?></textarea>
              <p style="color:aqua;"> Word Count:
                <span id="show" style="color:red;">100</span>
              </p>
              <span class="text-danger"> <?php echo $address_error; ?> </span>
            </div>

            <!------------------------Education-------------------->
            <div class="form-group">
              <div class="mb-3">
                <label for="education">Educational Qualification</label>
                <select name="education" class="custom-select d-block w-100" id="edu">
                  <option value="">Choose...</option>
                  <option value="BTech" <?php echo ($_POST["education"] == "BTech") ? 'selected' : ''; ?>>B.Tech</option>
                  <option value="MTech" <?php echo ($_POST["education"] == "MTech") ? 'selected' : ''; ?>>M.Tech</option>
                  <option value="BCA" <?php echo ($_POST["education"] == "BCA") ? 'selected' : ''; ?>>BCA</option>
                  <option value="MCA" <?php echo ($_POST["education"] == "MCA") ? 'selected' : ''; ?>>MCA</option>
                </select>
                <span class="text-danger"> <?php echo $education_error; ?> </span>
              </div>
            </div>

            <!------------------------Skill-------------------->
            <div class="form-group">
              <label>Skills:</label><br>
              <div class="form-check form-check-inline">
                <input type="checkbox" class="form-check-input" value="math" id="math" name="skill[]" <?php echo (in_array("math", $skills)) ? 'checked' : ''; ?>>
                <label class="form-check-label" for="math">math</label>
              </div>
              <div class="form-check form-check-inline">
                <input type="checkbox" class="form-check-input" value="science" id="science" name="skill[]" <?php echo (in_array("science", $skills)) ? 'checked' : ''; ?>>
                <label class="form-check-label" for="science">science</label>
              </div>
              <div class="form-check form-check-inline">
                <input type="checkbox" class="form-check-input" value="computer" id="computer" name="skill[]" <?php echo (in_array("computer", $skills)) ? 'checked' : ''; ?>>
                <label class="form-check-label" for="computer">computer</label>
              </div>
              <div class="form-check form-check-inline">
                <input type="checkbox" class="form-check-input" value="english" id="english" name="skill[]" <?php echo (in_array("english", $skills)) ? 'checked' : ''; ?>>
                <label class="form-check-label" for="english">english</label>
              </div>
              <div class="form-check form-check-inline">
                <input type="checkbox" class="form-check-input" value="chemistry" id="chemistry" name="skill[]" <?php echo (in_array("chemistry", $skills)) ? 'checked' : ''; ?>>
                <label class="form-check-label" for="chemistry">chemistry</label>
              </div>
              <span class="text-danger"> <?php echo $skill_error; ?> </span>
            </div>

            <!------------------------profile pic-------------------->
            <div class="form-group">
              <input type="file" name="photo" />
              <!-- <label for="exampleFormControlFile1">Profile Photo</label> -->
              <!-- <input name="photo" type="file" class="form-control-file" id="exampleFormControlFile1"> -->
              <!-- <input type="submit" name="photo" value="Upload" /> -->
              <span class="text-danger"> <?php echo $photoErr; ?> </span>
            </div>

            <!------------------------interests-------------------->
            <div class="form-group">
              <div class="mb-3">
                <label for="interests">Interests</label>
                <select multiple="multiple" name="interest[]" class="form-select d-block w-100" id="interests">
                  <option value="Cricket" <?php echo (in_array("Cricket", $interests)) ? 'selected' : ''; ?>>Cricket</option>
                  <option value="Football" <?php echo (in_array("Football", $interests)) ? 'selected' : ''; ?>>Football</option>
                  <option value="Tennis" <?php echo (in_array("Tennis", $interests)) ? 'selected' : ''; ?>> Tennis</option>
                  <option value="Basketball" <?php echo (in_array("Basketball", $interests)) ? 'selected' : ''; ?>>Basketball</option>
                  <option value="Boxing" <?php echo (in_array("Boxing", $interests)) ? 'selected' : ''; ?>>Boxing</option>
                </select>
                <span class="text-danger"> <?php echo $interestErr; ?> </span>
              </div>
            </div>

            <!------------------------link-------------------->
            <div class="form-group">
              <label>Professional link</label><br>
              <input type="text" name="prof" id="prof" value="<?= $prof ?>" id="prof" class="form-control" placeholder="https://meet.google.com/ksy-vgsc-pnk">
              <span class="text-danger"> <?php echo $prof_error; ?> </span>
            </div>

            <!------------------------submit button-------------------->
            <div class="form-group " style=" text-align:center">
              <button type="submit" name="submit" value="Submit" class='submit'>Submit</button>
            </div>

            <!-------------------------reset button--------------------->
            <div class="form-group " style=" text-align:center">
              <input type='reset' value='clear form' name='reset'>
            </div>
          </form>
        </div>
      </div>
      <div class="sig col col-lg-2 col-12"></div>
    </div>


    <!--------------------------------------OUTPUT--------------------------------------------->

    <div class="output_section mb-5" style=" margin-left:28%;">
      <?php
      echo "<h2 class='mb-5'>Your Input:</h2>";
      echo "<span class='output_text'>Name: </span>";
      echo $name;
      echo "<br>";
      echo "<span class='output_text'>Email: </span>";
      echo $email;
      echo "<br>";
      echo "<span class='output_text'>Contact: </span>";
      echo $mobile;
      echo "<br>";
      echo "<span class='output_text'>Gender: </span>";
      echo $gender;
      echo "<br>";
      echo "<span class='output_text'>About: </span>";
      echo $about;
      echo "<br>";
      echo "<span class='output_text'>Date of Birth: </span>";
      echo $date;
      echo "<br>";
      echo "<span class='output_text'>Skills: </span>";
      echo $skill;
      echo "<br>";
      echo "<span class='output_text'>Educational Qualification: </span>";
      echo $education;
      echo "<br>";
      echo "<span class='output_text'>Interests: </span>";
      echo $interest;
      echo "<br>";
      echo "<span class='output_text'>Website: </span>";
      echo "<a href>$prof;</a>";
      echo "<br>";
      echo "<span class='output_text'>Address: </span>";
      echo $address;
      echo "<br>";
      echo "<span class='output_text'>Image: </span>";
      echo $photo;
      echo "<br>";
      if ($error == 'false') {


        //VALUE SENDING TO NEXT PAGE FOR GET
        // header('Location: passdata.php?name=' . $name . '&email=' . $email . '&address=' .  $address . '&date=' . $date . '&photo=' . $photo .  '&mobile=' . $mobile . '&prof=' . $prof . '&education=' . $education . '&prof=' . $prof .  '&gender=' . $gender .  '&about=' . $about .  '&about=' . $about . '&skill=' . $skill . '&interest=' . $interest);


        //create session variable as user has valid details
        $uploaded = move_uploaded_file(
          $_FILES["photo"]["tmp_name"],
          "upload/" . $new_name
        );
        if ($uploaded) {
          $photo = $new_name;
        } else {
          $error = 'true';
          $photoErr = "File could not be uploaded";
        }
      }
      ?>
    </div>
    <!-- jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <!-- Font Awesome  -->
    <script src="https://kit.fontawesome.com/31875a1568.js" crossorigin="anonymous"></script>

    <!-- External JavaScript -->
    <script src="jQuery.js"></script>
    <script>
      function countWord() {

        // Get the input text value 
        var words = document
          .getElementById("word").value.length;
        var word1 = document
          .getElementById("word1").value.length;

        // Initialize the word counter 
        var count = 0;

        // Split the words on each 
        // space character  


        // Loop through the words and  
        // increase the counter when  
        // each split word is not empty 


        document.getElementById("show").innerHTML = 100 - words;
        document.getElementById("show1").innerHTML = 60 - word1;
        // Display it as output 

      }
    </script>

    <!-- BootStrap Script -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

</body>

</html>