$(function() {

    $("#txtDate").datepicker({
        changeMonth: true,
        changeYear: true,
        showOn: 'button',
        buttonImageOnly: true,
        buttonImage: 'image/calendar.gif',
        dateFormat: 'dd/mm/yy',
        yearRange: '1900:+0',
        onSelect: function(dateString, txtDate) {
            ValidateDOB(dateString);
        }
    });
});

/*

function ValidateDOB(dateString) {
    var lblError = $("#lblError");
    var parts = dateString.split("/");
    var dtDOB = new Date(parts[1] + "/" + parts[0] + "/" + parts[2]);
    var dtCurrent = new Date();
    lblError.html("Eligibility 18 years ONLY.")
    if (dtCurrent.getFullYear() - dtDOB.getFullYear() < 18) {
        return false;
    }

    if (dtCurrent.getFullYear() - dtDOB.getFullYear() == 18) {

        //CD: 11/06/2018 and DB: 15/07/2000. Will turned 18 on 15/07/2018.
        if (dtCurrent.getMonth() < dtDOB.getMonth()) {
            return false;
        }
        if (dtCurrent.getMonth() == dtDOB.getMonth()) {
            //CD: 11/06/2018 and DB: 15/06/2000. Will turned 18 on 15/06/2018.
            if (dtCurrent.getDate() < dtDOB.getDate()) {
                return false;
            }
        }
    }
    lblError.html("");
    return true;
}

var f = document.getElementsByTagName("form")[0];

f.addEventListener("submit", function(event) {
    event.preventDefault();
    // First Name
    if (this['firstName'].value.length <= 3) {
        this['firstName'].nextElementSibling.innerHTML = "Invalid";
    }
    // Email
    if (!((this['email'].value == "") || (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this['email'].value)))) {
        this['email'].nextElementSibling.innerHTML = "Invalid";

    }

    // Address
    if (this['address'].value.length <= 3) {
        this['address'].nextElementSibling.innerHTML = "Invalid";
    }
    //   //Education
    if (this['education'].value == "") {
        this['education'].nextElementSibling.innerHTML = "Invalid";
    }


    //   // Contact
    if (!((/^[0-9]*$/.test(this['contact'].value)) && (this['contact'].value.length == 6))) {
        this['contact'].nextElementSibling.innerHTML = "Invalid";
    }
});
*/