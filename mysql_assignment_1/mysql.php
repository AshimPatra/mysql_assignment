<?php

$servername = "localhost";
$username = "ashimp";
$password = "mindfire";
$dbname = "resume_data";
$tablename = "resume";

// Create connection
$conn = new mysqli($servername, $username, $password);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} else {
    if (create_db($conn, $dbname) === true) {
        if (create_table($conn, $tablename) === true)
            insert_table($conn, $tablename);
    }
}
header('Location: passdata.php');


$name = $_GET["name"];
$email = $_GET['email'];
$mobile = $_GET['mobile'];
$prof = $_GET['prof'];
$date = $_GET['date'];
$gender = $_GET['gender'];
$address = $_GET['address'];
$about = $_GET['about'];
$skill = $_GET['skill'];
$interest = $_GET['interest'];
$education = $_GET['education'];
$image = $_GET['photo'];


function insert_table($conn, $tablename)
{
    $name = $_GET["name"];
    $email = $_GET['email'];
    $mobile = $_GET['mobile'];
    $prof = $_GET['prof'];
    $date = $_GET['date'];
    $gender = $_GET['gender'];
    $address = $_GET['address'];
    $about = $_GET['about'];
    $skill = $_GET['skill'];
    $interest = $_GET['interest'];
    $education = $_GET['education'];
    $image = $_GET['photo'];

    echo ("interest = $interest");

    $sql = "INSERT INTO $tablename (name, email, mobile,interest,about,address,gender,skill,education,url,dob,image)
  VALUES ('$name', '$email', '$mobile','$interest','$about','$address','$gender','$skill','$education','$prof','$date','$image')";
    if ($conn->query($sql) === true) {
        echo "Data inserted  successfully";
    } else {
        die("Error creating table: " . $conn->error);
    }
}

function create_db(&$conn, $dbname)
{
    // Create database
    $sql = "CREATE DATABASE IF NOT EXISTS $dbname";
    if ($conn->query($sql) === TRUE) {
        echo "Database created successfully";
        $conn->select_db($dbname);
        return true;
    } else {
        die("Error creating database: " . $conn->error);
    }
}

function create_table($conn, $tablename)
{
    $sql = "CREATE TABLE IF NOT EXISTS $tablename (
    `name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
    `email` varchar(25) NOT NULL,
    `mobile` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
    `about` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
    `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
    `education` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
    `dob` varchar(25) DEFAULT NULL,
    `skill` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
    `url` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
    `image` longblob,
    `gender` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
    `interest` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;";

    if ($conn->query($sql) === TRUE) {
        return true;
    } else {
        die("ERROR: create table error. conn->error" . $conn->error);
    }
}
